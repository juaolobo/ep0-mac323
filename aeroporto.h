#ifndef AEROPORTO_H
#define AEROPORTO_H

#include "fila.h"
#include "avioes.h"
#include <iostream>
using namespace std;

class Pista {

	public :
		int estado;
		int tempo;
		Pista();

};

class Aeroporto {

	private :
		Pista pista[3];
	public:
		Aeroporto();
		~Aeroporto();
		int checkPista();
		void processaFila(Fila *);
		void atualizaAeroporto();
		void comunica(Fila *, Aviao *);
};

#endif
