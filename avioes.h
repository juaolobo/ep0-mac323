#ifndef AVIOES_H
#define AVIOES_H

#include <iostream>
using namespace std;

#define EM_USO 1
#define LIVRE 0

#define DECOLAGEM 0
#define POUSO 1

class Aviao {

	public :
		int estado;
		string compID;
		string lugar;
		long combustivel;
		long tempoTotal;
		long tempoEspera;
		bool emerg;
		Aviao *prox;
};

#endif