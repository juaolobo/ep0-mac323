#include <iostream>
#include <unistd.h>
#include "avioes.h"
#include "aeroporto.h"
#include "fila.h"

string Companhias[5] = {"AA", "BB", "CC", "DD", "EE"};
string Aeroportos[31] = {"AAA", "BBB", "CCC", "DDD", "EEE", "FFF", "GGG", "HHH", "III", "JJJ", "KKK", "LLL",
						"MMM", "NNN", "OOO", "PPP", "QQQ", "RRR", "SSS", "TTT", "UUU", "VVV", "WWW", "XXX", 
						"YYY", "ZZZ", "ABC", "DEF", "GHI", "JKL", "MNO"};

Aviao* criaAviao(int seed) {

	Aviao* aviao = new Aviao;
	srand(seed);
	aviao->estado = rand()%2;
	aviao->compID = Companhias[rand()%5];
	aviao->lugar = Aeroportos[rand()%31];
	aviao->combustivel = rand()%100;
	aviao->tempoTotal = rand();
	aviao->tempoEspera = 0;	
	aviao->emerg = rand()%2;
	aviao->prox = nullptr;

	return aviao;
}

void printAviao(Aviao *aviao) {
	cout << aviao->estado << aviao->compID << aviao->lugar << endl;
}

int main() {

	Fila fila;
	Aeroporto aeroporto;
	int i;
	int seed = 1;

	while (1) {

		Aviao * teste[3];
		teste[0] = criaAviao(seed++);
		aeroporto.comunica(&fila, teste[0]);

		teste[1] = criaAviao(seed++);
		aeroporto.comunica(&fila, teste[1]);

		teste[2] = criaAviao(seed++);
		aeroporto.comunica(&fila, teste[2]);

		fila.printSaida();

		while (aeroporto.checkPista() != -1)
			aeroporto.processaFila(&fila);

		fila.atualiza();
		aeroporto.atualizaAeroporto();

		usleep(5000000);
	}

	return 0;
}