#include "fila.h"
#include "avioes.h"

Fila::Fila() : n(0), ini(nullptr) {};

Fila::~Fila() {
	if (ini != nullptr && fim != nullptr) 
	{
		Aviao *aux;
		while(ini != fim)
		{
			if (ini != nullptr) {
				aux = ini->prox;
				delete ini;
			}
			ini = aux;
		}
	}
}

void Fila::queue(Aviao *aviao) {

	if (aviao != nullptr) {

		n++;

		if (!emptyQueue()) {
			fim->prox = aviao;
			fim = fim->prox;
			return;
		}

		fim = ini = aviao;
	}
}

/* funcao muda a posicao de algum aviao para primeiro, ele ja estando na fila ou nao */

void Fila::queueEmerg(Aviao *aviao) {
	Aviao *aux = ini;
	Aviao *ant;

	if (aviao != nullptr) {
		if (aviao->prox != nullptr) {		// se ele ja estava na fila (caso tenha passado 10% do tempo total)
				while (aux != aviao) {
					ant = aux;
					aux = aux->prox;
				}
				ant->prox = aux->prox;
				aviao->prox = nullptr;
			
		}
		aviao->prox = ini;
		ini = aviao;
		n++;
	}
}

bool Fila::emptyQueue() {
	return ini == nullptr;
}

Aviao* Fila::dequeue() {
		
	if(emptyQueue())
		return nullptr;
	
	Aviao *aux = ini;
	ini = ini->prox;
	aux->prox = nullptr;

	n--;

	return aux;
}

int Fila::nElem() {
	return n;
}

/* funcao retorna 1 se um aviao de emergencia pode passar na frente da fila */

int Fila::emergCheck() {

	Aviao *aux;
	int i = 1;
	if (emptyQueue())
		return 1;

	for (aux = ini; aux != fim; aux = aux->prox) {
		if (aux->combustivel < (nElem()-i)*3 + 3)
			return 0;
		i++;
	}
	return 1;
}

void Fila::atualiza() {

	if (emptyQueue())
		return;

	for (Aviao *aux = ini; aux != fim; aux = aux->prox) {

		if (aux->estado == POUSO)	{
			aux->combustivel--;
		}

		if (aux->tempoEspera == aux->tempoTotal*0.1){
			aux->emerg = 1;
			if (emergCheck())
				queueEmerg(aux);
		}

		aux->tempoEspera++;
	}
}

void Fila::printFila() {
	cout << ini->prox << " " << fim << endl;
}

void Fila::printSaida() {

	double mediaTempoPousos = 0, mediaCombPousos = 0, mediaTempoDec = 0, mediaCombDec = 0;
	int nPousos = 0, nDec = 0, nEmergs = 0;

	cout << "------------------------------------------------------------------" << endl;

	cout << "Temos " << nElem() << " avioe(s) na fila." << endl;
	cout << "Esses são os avioes esperando para pousar/decolar :" << endl << endl;

	for (Aviao *aux = ini; aux != nullptr; aux = aux->prox) {

		cout << aux->compID << "-" << aux->lugar;

		if (aux->estado == POUSO) {
			mediaTempoPousos += aux->tempoEspera;
			mediaCombPousos += aux->combustivel;
			nPousos++;
			cout << " [Esperando autorizacao para pousar];" << endl;
		}
		else {
			mediaTempoDec += aux->tempoEspera;
			mediaCombDec += aux->combustivel;
			nDec++;
			cout << " [Esperando autorizacao para decolar];" << endl;
		}

		if (aux->emerg == 1)
			nEmergs++;

		cout << endl;
	}

	if (nPousos != 0) {
		mediaTempoPousos = mediaTempoPousos/nPousos;
		mediaCombPousos =mediaCombPousos/nPousos;
	}
	else  {
		mediaTempoPousos = 0;
		mediaCombPousos = 0;
	}

	if (nDec != 0) {
		mediaTempoDec = mediaTempoDec/nDec;
		mediaCombDec = mediaCombDec/nDec;
	}
	else {
		mediaTempoDec = 0;
		mediaCombDec = 0;
	}

	cout << "Tempo medio de espera para pousar : " << mediaTempoPousos << endl;
	cout << "Tempo medio de espera para decolar : " << mediaTempoDec << endl;
	cout << "Media de combustivel para avioes esperando para pousar : " << mediaCombPousos << endl;
	cout << "Media de combustivel para avioes esperando para decolagem : " << mediaCombDec << endl;
	cout << "Avioes em estado de emergencia para pouso/decolagem : " << nEmergs << endl;
	cout << "------------------------------------------------------------------" << endl;

}