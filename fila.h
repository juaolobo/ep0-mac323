#ifndef FILA_H
#define FILA_H

#include "avioes.h"
#include <iostream>
using namespace std;


class Fila {
	private:

	public:
		Aviao *ini;
		Aviao *fim;
		int n;
		Fila();
		~Fila();
		void queue(Aviao *);
		void queueEmerg(Aviao *);
		Aviao* dequeue();
		bool emptyQueue();
		int nElem();
		int emergCheck();
		void atualiza();
		void printSaida();
		void printFila();
	
};

#endif 