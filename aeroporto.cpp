#include "fila.h"
#include "avioes.h"
#include "aeroporto.h"
#include <iostream>

using namespace std;

Pista::Pista() : estado(LIVRE), tempo(0) {};

Aeroporto::Aeroporto() {};

Aeroporto::~Aeroporto() {};

int Aeroporto::checkPista() {

	for (int i = 0; i < 3; i++)
		if (pista[i].estado == LIVRE)
			return i;	
	return -1;

}

void Aeroporto::processaFila(Fila *fila) {

	Aviao *aviao = fila->dequeue();
	if (aviao != nullptr) {

		int p = checkPista();

		if (p != -1) {

			if (aviao->estado == POUSO)
				cout << "O aviao " << aviao->compID << "-" << aviao->lugar << " esta pousando." << endl << endl;
			else
				cout << "O aviao " << aviao->compID << "-" << aviao->lugar << " esta decolando." << endl << endl;

			pista[p].estado = EM_USO;
		}
		delete aviao;
	}
}

void Aeroporto::comunica(Fila *fila, Aviao *aviao) {

	if (fila->emptyQueue()) {
		fila->queue(aviao);
		return;
	}

	if (aviao->emerg == 1) {
		if(fila->emergCheck() == 0)
			cout << "O aviao " << aviao->compID << "-" << aviao->lugar << " sera redirecionado" << endl << endl;

		else {
			cout << "Sera dada prioridade ao aviao " << aviao->compID << "-" << aviao->lugar <<  ", desculpe-nos pelo atraso" << endl;
			fila->queueEmerg(aviao);
		}
	}
	else 
		if (aviao->combustivel > (fila->nElem() * 3)){
			fila->queue(aviao);
		}
		else
			cout << "O aviao " << aviao->compID << "-" << aviao->lugar << "será redirecionado";
}

void Aeroporto::atualizaAeroporto() {
	for (int i = 0; i < 3; i++) {

		if (pista[i].tempo == 3) { 	// se algo tiver ocorrido e acabado o periodo de manutencao
			pista[i].estado = LIVRE;
			pista[i].tempo = 0;
		}
		if (pista[i].estado == EM_USO)
			pista[i].tempo++;
	}

}

