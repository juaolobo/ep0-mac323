CC = g++
CFLAGS = -Wall -Wno-unused-result -Wno-deprecated-declarations
RM = rm
#---------------------------------------------------------------
ep0: main.o aeroporto.o fila.o
	$(CC) main.o aeroporto.o fila.o -o ep0

main.o: main.cpp aeroporto.h fila.h avioes.h
	$(CC) $(CFLAGS) -c main.cpp

aeroporto.o: aeroporto.cpp fila.o aeroporto.h fila.h avioes.h
	$(CC) $(CFLAGS) -c aeroporto.cpp

fila.o: fila.cpp fila.h
	$(CC) $(CFLAGS) -c fila.cpp

clean:
	$(RM) *~ *.o

