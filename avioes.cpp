#include <iostream>
#include "avioes.h"
using namespace std;

int Aviao::getEstado() {
	return estado;
}

string Aviao::getCompID() {
	return compID;
}

string Aviao::getLugar() {
	return lugar;
}

long Aviao::getComb() {
	return combustivel;
}

long Aviao::getTempoTotal() {
	return tempoTotal;
}

long Aviao::getTempoEspera() {
	return tempoEspera;
}
bool Aviao::getEmerg() {
	return emerg;
}
